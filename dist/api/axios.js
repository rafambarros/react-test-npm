(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'axios', 'react-router'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('axios'), require('react-router'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.axios, global.reactRouter);
    global.axios = mod.exports;
  }
})(this, function (exports, _axios, _reactRouter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.apiMasterClient = exports.apiClient = undefined;

  var _axios2 = _interopRequireDefault(_axios);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var token = 'Bearer ' + localStorage.getItem('id_token');

  var config = {
    baseURL: 'http://localhost:9000',
    'Content-Type': 'application/json'
  };

  if (localStorage.getItem('id_token') !== '') {
    config['headers'] = { 'Authorization': token };
  }

  var apiClient = exports.apiClient = _axios2.default.create(config);

  apiClient.interceptors.response.use(undefined, function (err) {
    var res = err.response;
    if (res.status === 401 && res.config && !res.config.__isRetryRequest) {
      localStorage.setItem('id_token', '');
      _reactRouter.browserHistory.push('/login');
    }
  });

  var configMaster = config;

  configMaster['headers'] = { 'Authorization': 'Bearer ul7oJ8vGrg2Dfhg95uAG85NXGrrdblgx' };

  var apiMasterClient = exports.apiMasterClient = _axios2.default.create(config);

  apiMasterClient.interceptors.response.use(undefined, function (err) {
    var res = err.response;
    if (res.status === 401 && res.config && !res.config.__isRetryRequest) {
      localStorage.setItem('id_token', '');
      _reactRouter.browserHistory.push('/login');
    }
  });
});