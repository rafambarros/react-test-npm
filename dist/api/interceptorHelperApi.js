(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['./axios.js', 'react-router'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('./axios.js'), require('react-router'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.axios, global.reactRouter);
    global.interceptorHelperApi = mod.exports;
  }
})(this, function (_axios, _reactRouter) {
  'use strict';
});