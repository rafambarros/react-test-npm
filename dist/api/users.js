(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', './axios'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('./axios'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.axios);
    global.users = mod.exports;
  }
})(this, function (exports, _axios) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var ApiUsers = function () {
    function ApiUsers() {
      _classCallCheck(this, ApiUsers);
    }

    _createClass(ApiUsers, null, [{
      key: 'getList',
      value: function getList(options) {
        var queryString = '?';
        if (!(Object.keys(options).length == 0)) {
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = Object.keys(options.query)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var key = _step.value;

              var queryString = queryString + key + '=' + options.query[key];
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }
        }

        var instance = _axios.apiClient;
        return instance.get('/users' + queryString).then(function (response) {
          return response.data;
        });
      }
    }, {
      key: 'getUser',
      value: function getUser(userId, options) {

        var queryString = '?';
        if (!(Object.keys(options).length == 0)) {
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = Object.keys(options.query)[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var key = _step2.value;

              var queryString = queryString + key + '=' + options.query[key];
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }
        }

        var instance = _axios.apiClient;
        return instance.get('/users/' + userId + queryString).then(function (response) {
          return response.data;
        });
      }
    }, {
      key: 'addEdit',
      value: function addEdit(user) {
        var instance = _axios.apiMasterClient;
        console.log('user 123 => ', user);
        if (user.id) {
          return instance.put('/users', user).then(function (response) {
            return response.data;
          });
        } else {
          var _user = Object.assign({}, user, { password: 'inicial123' });
          return instance.post('/users', _user).then(function (response) {
            return response.data;
          });
        }
      }
    }, {
      key: 'delete',
      value: function _delete() {
        return new Promise(function (resolve) {
          setTimeout(function () {
            // do something here
            resolve();
          }, 500);
        });
      }
    }]);

    return ApiUsers;
  }();

  exports.default = ApiUsers;
});