(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'axios', './axios'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('axios'), require('./axios'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.axios, global.axios);
    global.userPayments = mod.exports;
  }
})(this, function (exports, _axios, _axios3) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _axios2 = _interopRequireDefault(_axios);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var ApiPayments = function () {
    function ApiPayments() {
      _classCallCheck(this, ApiPayments);
    }

    _createClass(ApiPayments, null, [{
      key: 'getUserPayment',
      value: function getUserPayment(id) {

        var instance = _axios3.apiClient;
        var gets = [];
        id.map(function (payment) {
          if (payment.invoice !== '') {
            var func = instance.get('/users/payments/' + payment.invoice);
            gets.push(func);
          }
        });
        return _axios2.default.all(gets).then(function (acct) {
          var userPayments = [];
          acct.map(function (response, index) {
            userPayments.push(response.data);
          });
          return userPayments;
        });
      }
    }]);

    return ApiPayments;
  }();

  exports.default = ApiPayments;
});