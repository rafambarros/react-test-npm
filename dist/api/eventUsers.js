(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', './axios'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('./axios'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.axios);
    global.eventUsers = mod.exports;
  }
})(this, function (exports, _axios) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var ApiEventUsers = function () {
    function ApiEventUsers() {
      _classCallCheck(this, ApiEventUsers);
    }

    _createClass(ApiEventUsers, null, [{
      key: 'getEventUsers',
      value: function getEventUsers(eventId) {

        var instance = _axios.apiClient;

        return instance.get('/event-competitors/events/' + eventId).then(function (response) {
          return response.data;
        });
      }
    }]);

    return ApiEventUsers;
  }();

  exports.default = ApiEventUsers;
});