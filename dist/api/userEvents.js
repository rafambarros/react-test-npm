(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'axios', './axios'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('axios'), require('./axios'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.axios, global.axios);
    global.userEvents = mod.exports;
  }
})(this, function (exports, _axios, _axios3) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _axios2 = _interopRequireDefault(_axios);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var ApiEvents = function () {
    function ApiEvents() {
      _classCallCheck(this, ApiEvents);
    }

    _createClass(ApiEvents, null, [{
      key: 'getUserEvents',
      value: function getUserEvents(userId) {

        var instance = _axios3.apiClient;

        return instance.get('/event-competitors/users/' + userId).then(function (response) {
          return response.data;
        });
      }
    }]);

    return ApiEvents;
  }();

  exports.default = ApiEvents;
});