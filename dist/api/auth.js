(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', './axios'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('./axios'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.axios);
    global.auth = mod.exports;
  }
})(this, function (exports, _axios) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var ApiAuth = function () {
    function ApiAuth() {
      _classCallCheck(this, ApiAuth);
    }

    _createClass(ApiAuth, null, [{
      key: 'signIn',
      value: function signIn(auth) {
        var instance = _axios.apiClient;
        var credentials = auth.email + ":" + auth.password;
        var token = 'Basic ' + btoa(credentials);
        instance.defaults.headers.common['Authorization'] = token;
        return instance.post('/auth', {
          'access_token': 'ul7oJ8vGrg2Dfhg95uAG85NXGrrdblgx'
        }).then(function (response) {
          instance.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
          return response.data;
        });
      }
    }, {
      key: 'signOut',
      value: function signOut() {
        var instance = _axios.apiClient;
        instance.defaults.headers.common['Authorization'] = '';
        return true;
      }
    }]);

    return ApiAuth;
  }();

  exports.default = ApiAuth;
});