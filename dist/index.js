(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', './MyComponent', './MyTest', './api/users'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('./MyComponent'), require('./MyTest'), require('./api/users'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.MyComponent, global.MyTest, global.users);
    global.index = mod.exports;
  }
})(this, function (exports, _MyComponent, _MyTest, _users) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.ApiUsers = exports.MyTest = exports.MyComponent = undefined;

  var _MyComponent2 = _interopRequireDefault(_MyComponent);

  var _MyTest2 = _interopRequireDefault(_MyTest);

  var _users2 = _interopRequireDefault(_users);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  exports.MyComponent = _MyComponent2.default;
  exports.MyTest = _MyTest2.default;
  exports.ApiUsers = _users2.default;
});