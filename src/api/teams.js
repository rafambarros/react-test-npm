// import axios from 'axios';
import {apiClient } from './axios';
import {apiMasterClient} from './axios';

// API Users static class
export default class ApiTeams {
  // get a list of users
  static getList(options) {
    var queryString = '?';
    if (!(Object.keys(options).length == 0)) {
      for(let key of Object.keys(options.query)) {
        var queryString = queryString + key + '=' + options.query[key];
      }
    }

    var instance = apiClient
    return instance.get('/teams'+ queryString).then(function (response) {
      return response.data;
    });

  }
  // get a user
  static getTeam(teamId, options) {

    var queryString = '?';
    if (!(Object.keys(options).length == 0)) {
      for(let key of Object.keys(options.query)) {
        var queryString = queryString + key + '=' + options.query[key];
      }
    }

    var instance = apiClient
    return instance.get('/teams/' + teamId + queryString).then(function (response) {
      return response.data;
    });

  }

  // add/edit a user
  static addEdit(team) {
    var instance = apiClient
    console.log('team 123 => ', team);
    if (team.id) {
      return instance.put('/teams', team).then(function (response) {
        return response.data;
      });
    }else {
      return instance.post('/teams', team).then(function (response) {
        return response.data;
      });
    }
  }
}
