import axios from 'axios';
import {apiClient} from './axios'

// API Users static class
export default class ApiEvents {
  // get a list of users
  static getUserEvents(userId) {

    var instance = apiClient

    return instance.get( '/event-competitors/users/' + userId ).then(function (response) {
      return response.data;
    });
  }
}
