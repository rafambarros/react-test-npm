import axios from 'axios';
import {apiClient} from './axios'

// API Users static class
export default class ApiPayments {
  // get a list of users
  static getUserPayment(id) {

    var instance = apiClient
    var gets = [];
    id.map((payment) =>
    {
      if (payment.invoice !== '') {
        let func = instance.get('/users/payments/' + payment.invoice)
        gets.push(func)
      }
    })
    return axios.all(gets)
      .then(function (acct) {
        var userPayments = [];
        acct.map((response, index) => {
          userPayments.push(response.data);
        });
        return userPayments;
      });
  }
}
