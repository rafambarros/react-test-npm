// import axios from 'axios';
import {apiClient} from './axios'
// API Auth static class
export default class ApiAuth {
  // SignIn
  static signIn(auth) {
    let instance = apiClient
    let credentials = auth.email + ":" + auth.password;
    let token = 'Basic ' + btoa(credentials);
    instance.defaults.headers.common['Authorization'] = token;
    return instance.post('/auth', {
      'access_token': 'ul7oJ8vGrg2Dfhg95uAG85NXGrrdblgx'
    }).then(function (response) {
      instance.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
      return response.data;
    });
  }

  // SignIn
  static signOut() {
    let instance = apiClient
    instance.defaults.headers.common['Authorization'] = '';
    return true
  }

}
