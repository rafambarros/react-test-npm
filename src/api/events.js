// import axios from 'axios';
import {apiClient } from './axios';
import {apiMasterClient} from './axios';

// API Users static class
export default class ApiEvents {
  // get a list of users
  static getList(options) {
    var queryString = '?';
    if (!(Object.keys(options).length == 0)) {
      for(let key of Object.keys(options.query)) {
        var queryString = queryString + key + '=' + options.query[key];
      }
    }

    var instance = apiClient
    return instance.get('/events'+ queryString).then(function (response) {
      return response.data;
    });

  }
  // get a user
  static getEvent(eventId, options) {

    var queryString = '?';
    if (!(Object.keys(options).length == 0)) {
      for(let key of Object.keys(options.query)) {
        var queryString = queryString + key + '=' + options.query[key];
      }
    }

    var instance = apiClient
    return instance.get('/events/' + eventId + queryString).then(function (response) {
      return response.data;
    });

  }

  // add/edit a user
  static addEdit(event) {
    var instance = apiClient
    console.log('event 123 => ', event);
    if (event.id) {
      return instance.put('/events', event).then(function (response) {
        return response.data;
      });
    }else {
      return instance.post('/events', event).then(function (response) {
        return response.data;
      });
    }
  }
}
