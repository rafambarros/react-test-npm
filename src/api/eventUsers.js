import {apiClient} from './axios'

// API EventUsers static class
export default class ApiEventUsers {
  // get a list of users of event
  static getEventUsers(eventId) {

    var instance = apiClient

    return instance.get( '/event-competitors/events/' + eventId ).then(function (response) {
      return response.data;
    });
  }
}
