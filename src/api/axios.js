import axios from 'axios'
import {browserHistory} from 'react-router'

const token = 'Bearer ' + localStorage.getItem('id_token');

let config = {
  baseURL: 'http://localhost:9000',
  'Content-Type': 'application/json'
}

if (localStorage.getItem('id_token') !== '') {
  config['headers'] = { 'Authorization': token }
}


export const apiClient = axios.create(config);

apiClient.interceptors.response.use(undefined, err => {
  let res = err.response;
  if (res.status === 401 && res.config && !res.config.__isRetryRequest) {
    localStorage.setItem('id_token', '');
    browserHistory.push('/login');
  }
})

var configMaster = config;

configMaster['headers'] = { 'Authorization': 'Bearer ul7oJ8vGrg2Dfhg95uAG85NXGrrdblgx' }

export const apiMasterClient = axios.create(config);

apiMasterClient.interceptors.response.use(undefined, err => {
  let res = err.response;
  if (res.status === 401 && res.config && !res.config.__isRetryRequest) {
    localStorage.setItem('id_token', '');
    browserHistory.push('/login');
  }
})
