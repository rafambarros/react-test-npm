import {apiClient} from './axios'
import {apiMasterClient} from './axios'

// API Users static class
export default class ApiUsers {
  // get a list of users
  static getList(options) {
    var queryString = '?';
    if (!(Object.keys(options).length == 0)) {
      for(let key of Object.keys(options.query)) {
        var queryString = queryString + key + '=' + options.query[key];
      }
    }

    var instance = apiClient
    return instance.get('/users'+ queryString).then(function (response) {
      return response.data;
    });

  }
  // get a user
  static getUser(userId, options) {

    var queryString = '?';
    if (!(Object.keys(options).length == 0)) {
      for(let key of Object.keys(options.query)) {
        var queryString = queryString + key + '=' + options.query[key];
      }
    }

    var instance = apiClient
    return instance.get('/users/' + userId + queryString).then(function (response) {
      return response.data;
    });

  }

  // add/edit a user
  static addEdit(user) {
    var instance = apiMasterClient
    console.log('user 123 => ', user);
    if (user.id) {
      return instance.put('/users', user).then(function (response) {
        return response.data;
      });
    }else {
      var _user = Object.assign({}, user, {password: 'inicial123'})
      return instance.post('/users', _user).then(function (response) {
        return response.data;
      });
    }

  }

  // delete a user
  static delete() {
    return new Promise(resolve => {
      setTimeout(() => {
        // do something here
        resolve();
      }, 500);
    });
  }
}
