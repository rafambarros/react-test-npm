// Demo component
// this is only example component
// you can find tests in __test__ folder

import React from 'react';

class MyTest extends React.Component {
    componentDidMount() {
        // some logic here - we only test if the method is called
    }
    render() {
        return (
            <div className="my-component">
                <button onClick={this.props.handleClick} type="button">MyTest</button>
            </div>
        )
    }
};

export default MyTest;
